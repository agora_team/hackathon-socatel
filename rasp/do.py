import sys
import RPi.GPIO as GPIO
import time

pastilla = int(sys.argv[1]) #que pastillero movemos? 1 o 2

#init pin
pin = 31 + pastilla
GPIO.setmode(GPIO.BOARD)   #Ponemos la Raspberry en modo BOARD
GPIO.setup(pin,GPIO.OUT)    #Ponemos el pin como salida
p = GPIO.PWM(pin,50)        #Ponemos el pin en modo PWM  i 50hz

#mover
p.start(15)
time.sleep(0.05)

#parar
p.stop()
GPIO.cleanup()

