#!/usr/bin/python
import mysql.connector as mariadb
import time as t
from datetime import datetime

try:
    import RPi.GPIO as GPIO
    host = "192.168.1.110"
except (RuntimeError, ModuleNotFoundError):
    import fake_rpigpio.utils
    fake_rpigpio.utils.install()
    GPIO = fake_rpigpio.RPi.GPIO
    host = "localhost"


def move_rail(rail):
    #init pin
    pin = 31 + int(rail)
    GPIO.setmode(GPIO.BOARD)   #Ponemos la Raspberry en modo BOARD
    GPIO.setup(pin,GPIO.OUT)    #Ponemos el pin como salida
    p = GPIO.PWM(pin,50)        #Ponemos el pin en modo PWM  i 50hz

    #mover 
    p.start(15)
    t.sleep(2)

    #parar
    p.stop()
    GPIO.cleanup()

def update_alarm():
    sql = "UPDATE Schedule SET alarm = %s WHERE id = %s"
    cursor2 = mariadb_connection.cursor()
    cursor2.execute(sql, (1, id))
    mariadb_connection.commit()  


def update_moved(id, today, name):
    print("Update")

    if(name == 'Medicament 1'):
        rail = 1
    else:
        rail = 2

    # insert last procesed
    sql = "UPDATE Schedule SET last_served = %s, alarm = %s WHERE id = %s"
    cursor2 = mariadb_connection.cursor(buffered=True)
    cursor2.execute(sql, (today, 0, id))
    mariadb_connection.commit()

    print("Serving rail: " + str(rail))
    move_rail(rail)
    print("End rail: " + str(rail))


mariadb_connection = mariadb.connect(host=host, user='root', password='rootpwd', database='testdb')

alarm = False
button_pressed = False

while True:
    t.sleep(1)
    today = datetime.today()
    print("Iteration")
    print(today)
    cursor = mariadb_connection.cursor(buffered=True)
    cursor.execute("SELECT * FROM Schedule", ())

    for (id, time_to, rail, last, has_alarm) in cursor:


        print(str(time_to) + ": " + rail)
        
        alarm = False

        if(last != None and today.date() == last.date()):
            print("Skip")
            print(last)
            # skip if already processed for today
            continue

        today_minutes = today.time().minute + today.time().hour * 60
        time_minutes = time_to.minute + time_to.hour * 60

        if(today_minutes >= time_minutes):
            # insert last procesed
            has_alarm = True
            update_moved(id, today, rail)

        if(button_pressed):
            has_alarm = False
            update_moved(id, today, rail)

        if(has_alarm):
            alarm = True

    mariadb_connection.commit()







    