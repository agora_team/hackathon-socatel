<?php

require_once('layout.php');
require_once('db.php');
require_once('forms.php');

session_start();
connect_db();

// get route if exists

if(isset($_GET['route'])) {
    // in a real application we should do security checks here.. this has multiple security issues
    // ex: check_user_allowed($_GET['route'], $_SESSION['user']);

    $_SESSION['route'] = $_GET['route'];
}

// process forms
if(isset($_GET['form'])) {
    // in a real application we should do security checks here.. this has multiple security issues
    // ex: check_user_allowed($_GET['form'], $_SESSION['user']);
    
    $_GET['form'](); // this is very ugly but very useful :)

    // after a form we always reload!
    close_db();
    header("Location: ".$_SERVER['PHP_SELF']);
    die();
}

// print

if(!isset($_SESSION['route'])) {
    require_once('login_page.php');
}
else {
    echo_header();
    
    require_once('pages/' . $_SESSION['route'] . '.php');
    echo_footer();
}

close_db();


?>