<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->


            <div class="content">
                <div class="container-fluid">
                    <div class="row" style="display:flex;justify-content:space-around">
                        <div class="card card-stats" style="width:40%">
                            <div class="card-header card-header-warning card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">healing</i>
                                </div>
                                <p class="card-category" style="font-size:20px">Medicaments receptats</p>
                                <h3 class="card-title" style="font-size:32px">1
                                </h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats" style="font-size:16px">
                                    <i></i> 3 dosis diàries
                                </div>
                            </div>
                        </div>
                        <div class="card card-stats" style="width:40%">
                            <div class="card-header card-header-success card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">alarm</i>
                                </div>
                                <p class="card-category" style="font-size:20px">Alarmes configurades</p>
                                <h3 class="card-title" style="font-size:32px">3</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats" style="font-size:16px">
                                    <i class="material-icons">date_range</i> Cada dia
                                </div>
                            </div>
                        </div>
                        <div class="card card-stats" style="width:40%">
                            <div class="card-header card-header-info card-header-icon">
                                <div class="card-icon" style="font-size:16px">
                                    <i class="material-icons">event</i>
                                </div>
                                <p class="card-category" style="font-size:20px">Dies consecutius prenent la medicació a l'hora</p>
                                <h3 class="card-title" style="font-size:32px">35</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats" style="font-size:16px">
                                    <i class="material-icons">check</i> Molt bé!
                                </div>
                            </div>
                        </div>
                        <div class="card card-stats" style="width:40%">
                            <div class="card-header card-header-danger card-header-icon">
                                <div class="card-icon">
                                    <i class="material-icons">local_hospital</i>
                                </div>
                                <p class="card-category" style="font-size:20px">Farmàcies properes</p>
                                <h3 class="card-title" style="font-size:32px">5</h3>
                            </div>
                            <div class="card-footer">
                                <div class="stats" style="font-size:16px">
                                    <i class="material-icons">directions_walk</i> A menys de 300m
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5" style="margin-left:62px">
                            <div class="card card-chart">
                                <div class="card-header card-header-success">
                                    <div class="ct-chart" id="dailySalesChart"></div>
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">Medicació restant</h4>
                                    <p class="card-category">
                                        <span class="text-success"><i class="fa fa-long-arrow-down"></i> 15 </span> càpsules gastades en la última setmana.</p>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">access_time</i> actualitzat fa 5 minuts
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>