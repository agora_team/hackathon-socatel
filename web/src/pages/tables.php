<!--
=========================================================
 Material Dashboard - v2.1.1
=========================================================

 Product Page: https://www.creative-tim.com/product/material-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 Licensed under MIT (https://github.com/creativetimofficial/material-dashboard/blob/master/LICENSE.md)

 Coded by Creative Tim

=========================================================

 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Horari de medicació</h4>
                    <p class="card-category"> Quan toca prendre's cada medicament?</p>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <th>
                                    ID
                                </th>
                                <th>
                                    Medicament
                                </th>
                                <th>
                                    Hores
                                </th>
                                <th>
                                    Última vegada
                                </th>
                                <th>
                                    Alarma
                                </th>
                            </thead>
                            <tbody>
                            <?php
                                $sql = "SELECT * FROM Schedule";
                                $result = $conn->query($sql);

                                global $conn;

                                if ($result->num_rows > 0) {
                                    // output data of each row
                                    while($row = $result->fetch_object()) {
                                        echo  "<tr><td>".
                                                $row->id
                                            ."</td>";
                                        echo '<td class="text-primary">'.$row->rail.'</td>';
                                        echo '<td class="text-primary">'.$row->time_to.'</td>';
                                        echo '<td class="text-primary">'.$row->last_served.'</td>';
                                        echo '<td class="text-primary">'.$row->alarm.'</td></tr>';
                                    }
                                }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title">Afegir medicament</h4>
            <p class="card-category">Omple els següents camps</p>
        </div>
        <div class="card-body">
            <form action="/index.php?form=insert_schedule_form" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                        <label class="bmd-label-floating">Medicament</label>
                        <input type="text" name="rail" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label class="bmd-label-floating">Hores</label>
                        <input type="text" name="time" class="form-control">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary pull-right">Afegir</button>
                <div class="clearfix"></div>
            </form>
    </div>

</div>